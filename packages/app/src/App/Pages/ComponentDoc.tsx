import React from "react"
import { DocPage } from "@flexure/flexui"
import { Link } from "react-scroll"
import { dummy } from "@flexure/common"

export const ComponentDoc: React.FC = () => {
	return (
		<DocPage
			renderTitle={(item: any) => (
				<Link activeClass="section_active" to={item.key} spy={true} smooth={true} offset={0} duration={500}>
					{item.displayName}
				</Link>
			)}
		>
			<DocPage.Section key="Key1" displayName="Section 1">
				<div>{dummy.dummyText}</div>
			</DocPage.Section>
			<DocPage.Section key="Key2" displayName="Section 2">
				<div>Test</div>
			</DocPage.Section>

			<DocPage.Section key="Key3" displayName="Section 3">
				<div>Test</div>
			</DocPage.Section>
		</DocPage>
	)
}
