import React from "react";
import { NavBar, Button } from "@flexure/flexui";
import { BaseNavBar, BaseButtonProps } from "@flexure/base-ui";

type AppButtonProps = BaseButtonProps;

const AppButton = ({ children, ...props }: AppButtonProps) => {
	return <Button {...props}>{children}</Button>;
};

export class AppBar extends React.PureComponent {
	static NearContainer = BaseNavBar.NearContainer;
	static FarContainer = BaseNavBar.FarContainer;
	static Button = AppButton;
	render() {
		return (
			<NavBar title="Flexure" alignment="vertical">
				{this.props.children}
			</NavBar>
		);
	}
}
