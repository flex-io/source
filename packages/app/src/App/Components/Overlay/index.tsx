import React from "react";
import _ from "lodash";
import { OverlayContainer } from "./Components/Container";
import { OverlayItem } from "./Components/Item";
import { OverlayContext } from "./context";
import { OverlayButton } from "./Components/Button";




export class Overlay extends React.Component {
	ref: any = React.createRef();
	static Container = OverlayContainer;
	static Item = OverlayItem;
	static Button = OverlayButton;

	contextValue = _.memoize((showOverlay, closeOverlay) => ({
		showOverlay,
		closeOverlay,
	}))(this.showOverlay.bind(this), this.closeOverlay.bind(this));

	showOverlay(args: string) {
		this.ref.current && this.ref.current.showOverlay(args);
	}

	closeOverlay(args: string) {
		this.ref.current && this.ref.current.showOverlay(args);
	}

	render() {
		const { children } = this.props;
		return (
			<OverlayContext.Provider value={this.contextValue}>
				{React.Children.map(children, (child: any) => {
					// Passing ref to execute function of a child
					if (child.type.displayName === "OverlayContainer") {
						return React.cloneElement(child, { ref: this.ref });
					} else {
						return child;
					}
				})}
			</OverlayContext.Provider>
		);
	}
}
