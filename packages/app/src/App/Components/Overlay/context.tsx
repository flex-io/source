import React from "react";

interface IContext {
	showOverlay: (args: string) => void;
	closeOverlay: () => void;
}
export const OverlayContext = React.createContext<Partial<IContext>>({});
