/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { OverlayContainerContext } from "./Container";
import { OverlayItemStyle } from "./Item.style";
import { OverlayContext } from "../context";

interface IOverlayComponent {
	position?: "left" | "right" | "bottom" | "top" | "popup" | "fullscreen";
}

const OverlayComponent: React.FC<IOverlayComponent> = (props) => {
	const { closeOverlay } = React.useContext(OverlayContext);
	const _ref = React.useRef<HTMLDivElement>(null);

	React.useEffect(() => {
		/**
		 * Event clicked outside of the element
		 */
		function _handleClickOutside(e: any) {
			if (_ref.current && !_ref.current.contains(e.target)) {
				if (typeof closeOverlay === "function") {
					closeOverlay();
				}
			}
		}

		// Bind the event listener

		document.addEventListener("mousedown", _handleClickOutside);
		return () => {
			// Unbind the event listener on clean up
			document.removeEventListener("mousedown", _handleClickOutside);
		};
	}, [_ref, closeOverlay]);

	return (
		<div css={OverlayItemStyle._dim}>
			<div ref={_ref} css={OverlayItemStyle._container}>
				{props.children}
			</div>
		</div>
	);
};

OverlayComponent.defaultProps = {
	position: "left",
};

export class OverlayItem extends React.PureComponent<{ key: any; activeKey?: any }> {
	[x: string]: any;
	static contextType = OverlayContainerContext;
	render() {
		const { children } = this.props;
		const key = this._reactInternalFiber.key;
		const { activeKey } = this.context;
		return key === activeKey ? <OverlayComponent>{children}</OverlayComponent> : <React.Fragment />;
	}
}
