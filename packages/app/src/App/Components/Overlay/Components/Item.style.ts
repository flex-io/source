import { css } from "@emotion/core";

export const OverlayItemStyle = {
	_container: css`
		display: flex;
		position: fixed;
		z-index: 110;
		left: 0;
		top: 0;
		bottom: 0;
		overflow: auto;
		transition: "0.5s";
		height: 100%;
		background-color: white;
		min-width: 500px;
	`,

	_dim: css`
		transform: translateZ(0);
		overflow: hidden;
		pointer-events: auto;
		background-color: rgba(0, 0, 0, 0.4);
		position: absolute;
		display: block;
		z-index: 100;
		left: 0px;
		right: 0px;
		top: 0px;
		bottom: 0;
	`,
};
