import { styled } from "@flexure/flexui";
import _ from "lodash";

interface IStyle extends Object {
    _style?: any
}

export const Styled = {
	Button: styled("button")<IStyle>`
		${(props) => {
			const sProps = _.omit(props, "children", "_style");
			return props._style?._button({ ...sProps });
		}}
	`,

	Container: styled("div")<IStyle>`
		${(props) => {
			const sProps = _.omit(props, "children", "_style");
			return props._style({ ...sProps });
		}}
	`,
};
