/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
export const OverlayDimContainer: React.FC<{ toggle: boolean; setToggle: any; _style: any }> = (props) => {
	const { toggle, setToggle, _style } = props;
	return (
		<React.Fragment>
			{toggle && <div css={_style?._dim} onClick={() => setToggle(false)} />}
			<div css={_style?._container}>{props.children}</div>
		</React.Fragment>
	);
};
