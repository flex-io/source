import React from "react";
import { Button, ButtonProps } from "@flexure/flexui";
import { iosSearch } from "react-icons-kit/ionicons/iosSearch";
import { Icon } from "react-icons-kit";
import { OverlayContext } from "../context";

type OverlayButtonProps = ButtonProps & { key: any };

export class OverlayButton extends React.PureComponent<OverlayButtonProps> {
	[key: string]: any;

	static contextType = OverlayContext;

	render() {
		const key = this._reactInternalFiber.key;
		const { showOverlay } = this.context;
		return (
			<Button onClick={() => showOverlay && showOverlay(key)}>
				<Icon size={16} icon={iosSearch} />
			</Button>
		);
	}
}
