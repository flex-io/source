import React from "react";
import _ from "lodash";

interface IContext {
	showOverlay: (args: string) => void;
	closeOverlay: () => void;
}
export const OverlayContainerContext = React.createContext<Partial<{ activeKey: string }>>({ activeKey: undefined });

export class OverlayContainer extends React.PureComponent {
	static displayName = "OverlayContainer";
	[x: string]: any;
	state = {
		activeKey: undefined,
	};
	constructor(props: any) {
		super(props);
		this.showOverlay = this.showOverlay.bind(this);
		this.closeOverlay = this.closeOverlay.bind(this);
	}

	contextValue = _.memoize((activeKey) => ({
		activeKey,
	}));

	showOverlay(args: string) {
		this.setState({ activeKey: args });
	}

	closeOverlay() {
		this.setState({ activeKey: undefined });
	}
	render() {
		const { children } = this.props;
		return (
			<OverlayContainerContext.Provider value={this.contextValue(this.state.activeKey)}>
				{children}
			</OverlayContainerContext.Provider>
		);
	}
}
