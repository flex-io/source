import React from "react";
import { AppBar } from "./Components/Bar";
import { Overlay } from "./Components/Overlay";
import { Button } from "@flexure/flexui";

const OverlayContainers = () => {
	return (
		<Overlay.Container>
			<Overlay.Item key="1">1</Overlay.Item>
			<Overlay.Item key="2">2</Overlay.Item>
		</Overlay.Container>
	);
};

const Navigation = () => {
	return (
		<AppBar>
			<AppBar.FarContainer>
				<Overlay.Button key="1" />
			</AppBar.FarContainer>
			<AppBar.NearContainer>
				<Button>n</Button>
			</AppBar.NearContainer>
		</AppBar>
	);
};

const AppContent = () => {
	return <div>Content</div>;
};

const App: React.FC = () => {
	return (
		<Overlay>
			<OverlayContainers />
			<div style={{ display: "flex" }}>
				<Navigation />
				<AppContent />
			</div>
		</Overlay>
	);
};

export default App;
