/** @jsx jsx */
import { jsx, css } from "@emotion/core"
import React from "react"
import { NavBar, ITheme } from "@flexure/flexui"
import { NavBarAlignment } from "@flexure/base-ui"
import { NavBarButton } from "./Components/NavBar/Button"
import { Icon } from "react-icons-kit"
import { androidSearch } from "react-icons-kit/ionicons/androidSearch"
interface IStylePrototypeApp {
	_container: any
}

const PrototypeAppStyle: IStylePrototypeApp = {
	_container: (theme: ITheme, args: { alignment?: NavBarAlignment }) => {
		console.log(args.alignment)
		return css`
			height: 100vh;
			display: flex;
			flex-direction: ${args.alignment === "vertical" ? "row" : "column"};
		`
	},
}

export type PrototypeAppProps = {
	appBarProps: any
}

export class PrototypeApp extends React.Component<PrototypeAppProps> {
	static defaultProps: PrototypeAppProps = {
		appBarProps: {
			title: "Flexure",
			alignment: "horizontal",
		},
	}

	render() {
		const { title, alignment } = this.props.appBarProps
		return (
			<div css={(theme) => PrototypeAppStyle._container(theme, { alignment })}>
				<NavBar
					_style={{
						_container: css`
							color: rgb(255, 255, 255);
						`,
					}}
					title={title}
					alignment={alignment}
				>
					<NavBar.NearContainer>
						<NavBarButton>
							<Icon icon={androidSearch} size={22} />
						</NavBarButton>
					</NavBar.NearContainer>

					<NavBar.FarContainer>
						<button>F</button>
					</NavBar.FarContainer>
				</NavBar>
				<p>Content</p>
			</div>
		)
	}
}
