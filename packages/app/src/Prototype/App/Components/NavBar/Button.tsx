import React from "react"
import { BaseButton, IStyleButton } from "@flexure/base-ui"
import { css } from "@emotion/core"


const ButtonStyle: IStyleButton = {
	_button: (theme) => css`
		font-family: inherit;

		align-items: center;
		color: inherit;
		cursor: pointer;
		display: flex;
		font-size: inherit;
		-webkit-box-pack: center;
		justify-content: center;
		line-height: 1;
		position: relative;
		height: 32px;
		width: 32px;
		background-color: transparent;
		border-width: 0px;
		border-radius: 50%;
		outline: none;
		padding: 0px;
	`,
}

export const NavBarButton: React.FC = ({ children }) => {
	return <BaseButton _style={ButtonStyle}>{children}</BaseButton>
}
